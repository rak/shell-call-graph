# shell-call-graph

A linux shell script for creating a static call graph of a linux shell script.

shell-call-graph outputs the graph description in
[DOT graph description language](https://en.wikipedia.org/wiki/DOT_%28graph_description_language%29),
which can be piped to your favorite DOT language processor, for example
[Graphviz](https://graphviz.org/) `dot` command.


shell-call-graph works well with my shell scripting style, and I believe it
works for the most people.  But shell-call-graph doesn't understand all possible
ways to write valid shell scripts, so it is possible that it doesn't work
for you.  Please read chapter [Notes](#notes) and [Examples](#examples) below.

![Call graph of shell-call-graph](test/call-graph-of-itself.svg "Function calls in script shell-call-graph")

The call graph of shell-call-graph (above) is created with the command below.

    shell-call-graph shell-call-graph | dot -Kcirco -Tsvg > test/call-graph-of-itself.svg



## Contents

[Licence](#licence)  
[Description](#description)  
[Installation](#installation)  
[About the code](#about-the-code)  
[Notes](#notes)  
[Examples](#examples)  



## Licence

Copyright Risto Karola 2022.  Licensed under the EUPL-1.2.
You may obtain a copy of the licence at
https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12.



## Description

### Usage

    shell-call-graph [options] FILE

shell-call-graph reads shell script FILE, finds function definitions and
function calls in FILE, and outputs a static call graph description in DOT
graph description language.

When run without options, the output goes to stdout.  With `-o`, `-O` or `-v`
option, the output is written to a file.


### Options

    --copyright
           display version, copyright and licence information and exit
    -f, --functions-only
           display functions defined in FILE with line numbers and exit
    -h, --help
           display help text and exit
    --licence
           display version, copyright and licence information and exit
    -o, --output=OUTPUT-FILE
           write the graph description to OUTPUT-FILE; quit, if it already exists
    -O OUTPUT-FILE
           write the graph description to OUTPUT-FILE, overwriting if it exists
    -v, --verbose
           display processing information; write the graph description to
           a file; if no option -o or -O is used, create the file with mktemp
           command
    --version
           display version, copyright and licence information and exit


### Exit codes

- 0 : No problems detected

- 1 : Problems detected, message output to stderr



## Installation

shell-call-graph is a normal single-file shell script.  No surprises in
installation.  Below are two examples for installation, but you may do
something else that works better for you.


### 1. Simple installation

Place shell-call-graph to a folder of you choice.  Make the script executable.
You may also place the man page shell-call-graph.1.gz to a folder.
To execute the script and to read the man page, provide the paths to them:

    /path/to/script/folder/shell-call-graph FILE
    man -l /path/to/man/folder/shell-call-graph.1.gz


### 2. Simple execution

Place the script shell-call-graph to a folder in $PATH, for example in
`~/.local/bin` (if the folder is not listed in $PATH you need to add it there).
Make the script executable.  When executing the script, just type its name.

Copy the man page shell-call-graph.1.gz to a folder, where the system finds it.
In my Debian based system I am using `~/.local/bin/man/man1/`.  Read it as any
other man page.



## About the code

shell-call-graph is a shell script.  It is coded to run on Dash shell (Debian
Almquist shell) for portability.  Note that the script uses `local` command
in functions.

It is tested on Debian based system with dash version 0.5.11+git20210903...
and with bash shell (Bourne Again SHell), version 5.1-5+b1.



## Notes

1. shell-call-graph expects function definitions in FILE to follow either of
   the two common forms:

   * Function is defined with the reserved word `function`
     ```
     function funcname [()] [{]
     [{]
         <function body>
     }
     ```
     The first line: The word `function` is at the beginning of the line.
     There may be parentheses `()` after the function name.  The opening brace
     `{` may be either on this line or below.  The function body does not start
     on this line, but below.

     Function body is between braces `{` and `}`, below the first line.

     The last line: The closing brace `}` is at the beginning of the line.

   * Function is defined with name + parentheses `()`
     ```
     funcname() [{]
     [{]
         <function body>
     }
     ```
     The first line: The function name is at the beginning of the line.  The
     opening brace `{` may be either on this line or below.  The function body
     does not start on this line, but below.

     Function body is between braces `{` and `}`, below the first line.

     The last line: The closing brace `}` is at the beginning of the line.

2. shell-call-graph does not work correctly with all valid ways to define
   a function:

   * The function body should be between braces `{` and `}`.  If any other
     compound commands (see `man bash`) are used, then the function end is not
     found.

   * Function definition start, `funcname()` or `function funcname`, is found
     only if it is at the beginning of the line.

   * The closing brace `}` of a function definition is found only if it is at
     the beginning of the line.

3. The search for function calls in a function definition starts from the
   second line.  A function call on the first line is missed.

4. The search for function calls in the main program starts after the last
   function definition.  A function call in main program is missed if the
   call is in between function definitions.

5. Function calls in quoted eval arguments are not recognized; they are missed.
   Unquoted function calls in eval arguments are found.

6. If a command has a function name as an unquoted parameter, then it is falsely
   taken as a function call.

7. Command `.` or `source` in FILE is not recognized; the sourced file is not
   read.  Any function definition or function call in sourced files is missed.



## Examples
  
Do not create a call graph, but only display defined functions and their line
numbers in file myscript.sh:

    shell-graph -f myscript.sh

Output the call graph description of myscript.sh to the screen:

    shell-call-graph myscript.sh

Show defined functions and processing progress on the screen; output the call
graph description of myscript.sh to a temporary file; display the filename at
the end of processing:

    shell-call-graph --verbose myscript.sh

(This requires dot command (graphviz) being installed.)  Use dot command to
convert the graph description to a scalable vector graphic file theGraph.svg:

    shell-call-graph myscript.sh | dot -Tsvg > theGraph.svg


### Script test/test_1.sh

This script shows

- The various function definition formats shell-call-graph understands.

- How a variable, which has the same name as a function, is not falsely
  interpreted as a function call, see function function_format_15.

- How function name in quoted strings is not falsely interpreted as a function
  call, see functions function_format_16, function_format_17 and
  function_format_18.

- How recursive function is shown in the output, see function_format_09;
  also, how functions calls may form a loop: calls from function_format_12 to
  function_format_14 to function_format_15 and back to function_format_12.

- How a function, which is defined, but never called is shown in the output:
  - function_format_03 is never called, and it is not calling any functions
  - function_format_06 is never called, but it is calling another function

![Call graph of test/test_1.sh](test/test_1.svg "Function calls in script test/test_1.sh")

The call graph above is generated using the command 

    shell-call-graph test/test_1.sh | dot -Tsvg > test/test_1.svg


### Scripts test/test_2a.sh ... test/test_2d.sh

Script test/test_2a.sh is perfectly executable on bash (try it, if you like),
but it has a few places in its code where shell-call-graph can not do
a perfect job.  Below we go through those places in the script, and modify it
until shell-call-graph outputs the correct result.

1. Round one

   - Command:

         shell-call-graph test/test_2a.sh

   - Output:

         Error: Found more function starting lines than function ending lines
                Function starting lines:
                10 19 24 36 41 46 49
                Function ending lines:
                21 21 27 38 43

   - Explanation: It is quicky seen from the error message that the last
     function (starting on line 49) does not have a counterpart in the list of
     ending lines.  On line 51 in test_2a.sh the function closing brace '}'
     has a tab character before it, which is why shell-call-graph can not
     find it.

   - Fixed in script test/test_2b.sh: the tab character at the beginning of
     line 51 is removed.

2. Round two

   - Command:

         shell-call-graph test/test_2b.sh

   - Output:

         Error: Function horse end is found on
                line 21, which is after the beginning of function
                sheep on line 19
                Function starting lines:
                10 19 24 36 41 46 49
                Function ending lines:
                21 21 27 38 43 51 51

   - Explanation: Function horse end is not found because the closing
     brace '}' on line 16 in test_2b.sh has a space before it. That is why
     shell-call-graph finds line 21 being the end of two functions.  Also,
     line 51 is listed as the end of two functions because the correct end of
     function oneliner is not found ('}' isn't at the beginning of the line).

   - Fixed in script test/test_2c.sh: 

     * The space before the closing brace of function horse on line 16 is
       removed.

     * The function oneliner on line 46 is split to two lines: a newline is
       added just before the closing brace '}'.

3. Round three

   - Command:

         shell-call-graph test/test_2c.sh

   - Output:

         digraph {
           horse -> horse
           cat -> horse
           cat -> sheep
           liner_3 -> dog
           Main -> cat
           Main -> oneliner
         
           /* Not called & not calling: */
           duck
         }

   - Explanation: Now shell-call-graph outputs a call graph description and is
     happy with it.  It shouldn't be.

     * Function cow is missing from the graph description.  The reason:
       `cow()` (line 31 in test_2c.sh) is not at the beginning of the line;
       shell-call-graph doesn't find the definition of function cow.

     * The output lists `cat -> sheep`, this call doesn't exist. Function cat
       has an unquoted word `sheep` as a parameter of a function call (line 37
       in test_2c.sh), shell-call-graph takes the parameter as being
       a function call.

     * Call `liner_3 -> duck` (line 50 in test_2c.sh) is missing from the
       graph description.  That is because function calls are not searched for
       on the first line of a function definition.

     * Call `Main -> sheep` (line 29 in test_2c.sh) is missing from the graph
       description, because function calls in the main program are searched for
       only after the last function definition.

     * Call `Main -> liner_3` (line 57 in test_2c.sh) is missing from the
       graph description.  The call is in quoted `eval` string.
       It is not recognized.

   - Fixed in script test/test_2d.sh: 

     * The indentation of the function cow is removed on line 29, `cow()` is
       at the beginning of the line in test_2d.sh.

     * Added quotes around parameter `sheep` on line 37.

     * On line 50: Added a newline, so that the function body is below
       the first line of the function definition.

     * Moved main program call to sheep after the last function definition.

     * Unquoted the word `liner_3` on line 57.

4. Round four

   - Command:

         shell-call-graph test/test_2d.sh

   - Output:

         digraph {
           horse -> horse
           cat -> horse
           liner_3 -> duck
           liner_3 -> dog
           Main -> sheep
           Main -> cow
           Main -> cat
           Main -> oneliner
           Main -> liner_3
         }

   - Explanation: The output of shell-call-graph is now what it should be.

