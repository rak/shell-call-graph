#!/bin/bash

printf "+-------------------+\n"
printf "| ANIMAL DICTIONARY |\n"
printf "+---------+---------+\n"
printf "| ENGLISH | FINNISH |\n"
printf "+---------+---------+\n"


horse () {
    if [ "$1" != "polle" ]; then
        printf "|  horse  | $(horse "polle") |\n"
    else
        printf "hevonen"
    fi
 }


function sheep(){
    printf "|  sheep  | lammas  |\n"
}


function duck
{
    printf "|  duck   |"
}

sheep

    cow () {
        printf "|   cow   |  lehmä  |\n"
    }


cat () {
    printf "|   cat   |  kissa  |\n$(horse sheep)\n"
}


dog() {
    printf "|   dog   |  koira  |\n"
}


oneliner() { printf "|  mouse  |  hiiri  |\n"; }


function liner_3 { duck; printf "  ankka  |\n"
   dog
	}


# MAIN

eval "liner_3"
echo "$(cat)"
cow
oneliner
printf "+---------+---------+\n"

