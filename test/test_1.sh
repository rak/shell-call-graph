#!/bin/bash

# Test script for testing bash-call-script.sh

R="\033[0;0m"  # Reset foreground and background colors

function_format_01 () {
    local C="\033[36m"  # Cyan text
    echo -e "$C(01) Hello!"
}


function_format_02 (){  # This function is never executed
    echo "(02) Hi there :)"
}


function_format_03 ()
{
    echo "(03) This function is never called, this text is never seen."
}


function_format_04 ()  # This function is never executed
{   echo " (04) I'm calling function 02: `function_format_02 dummy`(04)"
}


function_format_05() {
    local C="\033[35m"  # Magenta text
    echo -e "$C(05) Calling function_format_01: $(function_format_01)$C (05)"
}


function_format_06(){
    echo " (06) I am never called, but calling functions 01 and 04."
    function_format_01
    if [ "$?" -eq 0 ]; then echo " (06) "; function_format_04; fi
    echo "(06)"
}


function_format_07()
{
    local C="\033[32m"  # Green text
    # This text is in comment, is not a call: function_format_03
    echo -e "$C(07) Hello from function_format_07."
}


function_format_08()
{   local C="\033[33m"  # Yellow text
    echo -e "$C(08) Zero-eight!"
    # function_format_03 is not called here either
}


function function_format_09 () {
    local C="\033[34m"  # Blue text
    echo -n -e "$C(09) "
    if [ "$1" != "nomore" ]; then
        echo "calling function_format_05 and myself."
        function_format_05; function_format_09 "nomore"
        echo -e "(09)"
    else
        echo -n "I am function_format_09. "
    fi
}


function function_format_10 (){
    local C="\033[31m"  # Red text
    echo -e "$C(10) Calling three functions: 07, 08 and 09: $(function_format_07)"
    echo -e "$(function_format_08)`function_format_09`$C (10)$R"
}


function function_format_11 ()
{
    echo "(11) This is not shown as the call directs the output elsewhere"
}


function function_format_12 ()
{   local C="\033[33m"  # Yellow text
    echo -n -e "$C(12) 10 + 2. "
    if [ -n "$1" ]; then
        echo -n -e "Loop: calling 14"
        function_format_14
        echo -e "$C(12)"
    fi
}


function function_format_13() {
    local C="\033[32m"  # Green text
    echo -e "$C(13) Calling 11 (which displays nothing) and 12."
    function_format_12 "$(function_format_11)"
    echo -e "$C(13)$R"
}


function function_format_14(){
    local C="\033[35m"  # Magenta text
    echo -e "$C(14) Fourteen calling fifteen 15."
    function_format_15
    echo -e "$C(14)"
}


function function_format_15()
{
    local C="\033[36m"  # Cyan text
    function_format_03=1  # This is a variable, not function call
    echo -e "$C(15) Calling function_format_12."
    if [ $function_format_03 -eq 1 ]; then function_format_12; fi
    echo -e "$C(15)"
}


function function_format_16()
{   local C="\033[31m"  # Red text
    echo -e "$C(16) This is not a call: function_format_03 !!!$R"
}


function function_format_17 {
    local C="\033[34m"  # Blue text
    echo -e "$C"'(17) $(function_format_03) is not called here either'"$R"
}


function function_format_18
{
    local C="\033[32m"  # Green text
    echo -e "$C"'(18) Not calling this: function_format_03'"$R"
}


function function_format_19
{
    echo "(19) Calling function 10: $(function_format_10)"
    echo "(19) Calling function 13: `function_format_13`"
    echo -n "(19) Calling function 16."; function_format_16
    echo "(19) Calling function 17: " "$(function_format_17)"
    echo "(19) Calling function 18: " "`function_format_18`" "(19)"
}


# MAIN
M="\033[7m"  # Inverse foreground and background
echo -e "$M(Main) Calling 19.$R"; function_format_19; echo -e "$M(Main)$R"

